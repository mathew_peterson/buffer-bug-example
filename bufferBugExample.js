/**
 * Created by matp on 10/6/15.
 */

var Buffer = require('buffer').Buffer;

function BufferBugExampleRequests() {

}

BufferBugExampleRequests.prototype.exampleRequest = function() {
    var host = "https://p01-mobilebackup.icloud.com";
    var url = "/mbs/422486339/108564fc81f797971e56c0fd23abe742f5acf0aa/24/getFiles";
    var method = "POST";

    var headers = {
        "Authorization":"X-MobileMe-AuthToken NDIyNDg2MzM5OkFRQUFBQUJXRTc0OEVBYW13dllXT09IdmUwcGgvVGE1ZkRwVnRHZz0=",
        "Content-Length":46,
        "Content-Type":"application/vnd.com.apple.mbs+protobuf",
        "User-Agent":"MobileBackup/5.1.1 (9B206; iPhone3,1)",
        "X-Apple-MBS-Protocol-Version":"1.7",
        "X-MMe-Client-Info":"<iPhone2,1> <iPhone OS;5.1.1;9B206> <com.apple.AppleAccount/1.0 ((null)/(null))>"
    };

    var body = new Buffer([22,
        10,
        20,
        61,
        13,
        126,
        95,
        178,
        206,
        40,
        136,
        19,
        48,
        110,
        77,
        70,
        54,
        57,
        94,
        4,
        122,
        61,
        40,
        22,
        10,
        20,
        49,
        187,
        123,
        168,
        145,
        71,
        102,
        212,
        186,
        64,
        214,
        223,
        182,
        17,
        60,
        139,
        97,
        75,
        228,
        66,
        46]);

    return Parse.Cloud.httpRequest({
        url:(host + url),
        method : method,
        body : body,
        headers : headers
    });
};

module.exports = BufferBugExampleRequests;