var BufferBugExample = require('cloud/bufferBugExampleBundle.js');

Parse.Cloud.define("buffer_bug_example", function(request, response) {
  var requests = new BufferBugExample();
  requests.exampleRequest().then(function (httpResponse) {
    if(httpResponse.error) {
      response.error("Error");
    }
    else {
      response.success("success");
    }
  });
});
