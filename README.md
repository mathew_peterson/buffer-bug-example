Steps to reproduce bug at https://developers.facebook.com/bugs/703521836449540

The exampleRequest in the bufferBugExample.js function specifies the exact request that I refer to in the bug.

In my app the code is browserified using the folowing command.

browserify --standalone 'BufferBugExample' -o bufferBugExampleBundle.js bufferBugExample.js

main.js uses the browserified version in bufferBugExampleBundle.js

Currently the cloud code for my 'mytrue-dev' app has the example I provide here running under the 'buffer_bug_example' function name.

The result is:

{
    "code": 141,
    "error": "Uncaught Error: Don't know how to convert httpRequest body Object to application/vnd.com.apple.mbs+protobuf. To send raw bytes, please assign httpRequest body to a Buffer object containing your data."
}